#ifdef _WIN32
    #define _CRT_SECURE_NO_WARNINGS
#endif

#include <assert.h>
#include "network.h"
#include "system.h"
#include "protocol.h"
#include "generic.h"

#define reset_error()       SET_ERROR(_err, 0)
#define HS_MAX_TRIES        (100)

Error _err = { ._module = "server" };
volatile int net_writable = 0;
volatile AppState state = Disconnected;
_Buffer ps_buf;

void usage_app(void) {
    #ifndef _WIN32
        const char *app_name = APP_NAME;
    #else
        const char *app_name = APP_NAME".exe";
    #endif
    
    printf("Usage:\n");
    printf("\t%s <port>\n", app_name);
    printf("\tport - server port [1024..65535]\n");
}

void usage_commands(void) {
    printf("Usage:\n");
    printf("\tps         - get process list\n");
    printf("\tkill <pid> - kill process with pid\n");
}

uint16_t scan_port(char *argv) {
    int port;
    int ret = sscanf(argv, "%d", &port);
    if (!ret || port < 1024 || port > 65535) {
        LOGE(_err, "Invalid port!\n");
        return 0;
    }
    return (uint16_t)port;
}

/* Network callbacks */
void on_connect(void *arg);
void on_writable(void *arg);
void on_receive(void *arg);
void on_disconnect(void *arg);
void on_error(void *arg);

/* Server dependent functions */
int parse_command(char *cmd_in, int *cmd_out, PID *pid_out);
int read_packet(Packet **packet);
int send_handshake(void);
int send_request_ps(void);
int send_request_kill(PID pid);

int main(int argc, char *argv[]) {
    reset_error();

    int ret = 0;
    NetworkParam param;
    
    /* User command input buffer */
    char *cmd_in;

    /* It's not very accurate, I shouldn't measure time this way */
    int hs_tries = 0;   

    /* Check command line arguments */
    if (argc < 2) {
        LOGE(_err, "Server port is not specified!\n");
        usage_app();
        return 1;
    }

    /* Read server port */
    param.port = scan_port(argv[1]);
    if (!param.port) {
        usage_app();
        return 1;
    }
    
    /* Network initialization */
    param.mode = NetworkServer;
    param.cb.on_connect = &on_connect;
    param.cb.on_writable = &on_writable;
    param.cb.on_receive = &on_receive;
    param.cb.on_disconnect = &on_disconnect;
    param.cb.on_error = &on_error;

    if (!network_init(&param)) {
        network_perror();
        goto exit;
    }

    for(;;) {
        if (!network_poll()) {
            network_perror();
            goto exit;
        }

        switch(state) {
            case Disconnected:
            break;
            case HandshakeWait: {
                if (hs_tries >= HS_MAX_TRIES) {
                    LOGD(_err, "The client did not respond to the handshake!\n");
                    network_disconnect();
                    hs_tries = 0;
                    break;
                }

                Packet *packet;
                if ((ret = read_packet(&packet)) == 1) {
                    if (packet->type == ClientHSPacket && packet->handshake.id == PACKET_HANDSHAKE) {
                        LOGD(_err, "Client handshake accepted!\n");
                        state = Handshake;
                        hs_tries = 0;
                    }
                    else {
                        LOGD(_err, "Client sent a wrong handshake!\n");
                        network_disconnect();
                        hs_tries = 0;
                    }
                    packet_free_all(packet);
                }
                else if (ret < 0) goto exit;

                hs_tries++;
            }
            break;
            case Handshake:
                if (!send_handshake()) {
                    system_perror();
                    goto exit;
                }
                state = Established;
            break;
            case Established: {
                /* Read packets from server*/
                Packet *packet;
                if ((ret = read_packet(&packet)) == 1) {
                    switch(packet->type) {
                        case PLBeginPacket:
                        case PLChunkPacket:
                        case PLEndPacket:
                            if (!buffer_append(&ps_buf, packet->process_list.data)) {
                                LOGE(_err, "Memory allocation error!\n");
                                goto exit;
                            }
                            if (packet->type == PLEndPacket) {
                                printf("\nBEGIN >>>>>\n\n%s\n<<<<< END\n", ps_buf.data);
                                buffer_reset(&ps_buf);
                            }
                        break;
                        case ResponsePacket:
                            LOGM(_err, "Client: %s\n", packet->response.data);
                        break;
                    }
                    packet_free_all(packet);
                }
                else if (ret < 0) goto exit;

                /* Poll stdin, parse commands and send requests */
                if (net_writable) {
                    if ((ret = read_console_line(&cmd_in)) > 0) {
                        int cmd;
                        PID pid;
                        if (parse_command(cmd_in, &cmd, &pid)) {
                            switch(cmd) {
                                case GetPLPacket:
                                    if (!send_request_ps()) {
                                        protocol_perror();
                                        goto exit;
                                    }
                                break;
                                case PKillPacket:
                                    if (!send_request_kill(pid)) {
                                        protocol_perror();
                                        goto exit;
                                    }
                                break;
                            }
                        }
                    }
                    else if (ret < 0) {
                        system_perror();
                        goto exit;
                    }
                }
            }
            break;
        }

        system_sleep(1);
    }

exit:
    network_deinit();
    return 1;
    /* I don't free any allocated memory, it doesn't matter here */
}

void on_connect(void *arg) {
    LOGM(_err, "Client connected!\n");
    receive_buf_reset();
    state = HandshakeWait;
}

void on_writable(void *arg) {
    net_writable = 1;
}

void on_receive(void *arg) {
    /* After exiting the function, the data in the buffer may be lost */
    NetworkBuffer *received = (NetworkBuffer*)arg;
    LOGD(_err, "Received %d bytes from client!\n", received->len);
    if (!receive_buf_store(received->data, received->len)) {
        assert(0);
    }
}

void on_disconnect(void *arg) {
    LOGM(_err, "Client disconnected!\n");
    state = Disconnected;
}

void on_error(void *arg) {
    network_perror();
    network_deinit();
    exit(1);
}

int parse_command(char *cmd_in, int *cmd_out, PID *pid_out) {
    char cmd[8];
    const char *cmd_ps = "ps";
    const char *cmd_kill = "kill";

    int scan_ret = sscanf(cmd_in, "%4s %u", cmd, (uint32_t*)pid_out);
    size_t cmd_len = strlen(cmd);

    int is_cmd_ps = (scan_ret == 1) & (cmd_len == strlen(cmd_ps)) & (!strncmp(cmd, cmd_ps, cmd_len));
    int is_cmd_kill = (scan_ret == 2) & (cmd_len == strlen(cmd_kill)) & (!strncmp(cmd, cmd_kill, cmd_len));

    if (is_cmd_ps) {
        LOGD(_err, "Command: 'ps'\n");
        *cmd_out = GetPLPacket;
        return 1;
    }
    else if (is_cmd_kill) {
        LOGD(_err, "Command 'kill %u'\n", (uint32_t)(*pid_out));
        *cmd_out = PKillPacket;
        return 1;
    }

    LOGE(_err, "Unknown command '%s'!\n", cmd_in);
    usage_commands();
    return 0;
}

int read_packet(Packet **packet) {
    uint8_t *data;
    uint16_t len = receive_buf_peek(&data);
    
    *packet = packet_unpack(data, len);
    if (!(*packet)) {
        if (protocol_gerror() != PROTOCOL_ERR_BAD_PACKET) {
            protocol_perror();
            return -1;
        }
        return 0;
    }
    
    receive_buf_erase((*packet)->len);
    return 1;
}

int send_handshake(void) {
    static uint8_t *raw_hs = NULL;
    static uint16_t raw_hs_len = 0;

    if (!raw_hs) {
        Packet *packet_hs = packet_create();
        if (!packet_hs) return 0;

        packet_hs->type = ServerHSPacket;
        raw_hs = packet_pack(packet_hs);
        if (!raw_hs) return 0;

        raw_hs_len = packet_hs->len;
        packet_free(packet_hs);
    }

    network_send(raw_hs, raw_hs_len);
    net_writable = 0;
    return 1;
}

int send_request_ps(void) {
    static uint8_t *raw_ps = NULL;
    static uint16_t raw_ps_len = 0;

    if (!raw_ps) {
        Packet *packet_ps = packet_create();
        if (!packet_ps) return 0;

        packet_ps->type = GetPLPacket;
        raw_ps = packet_pack(packet_ps);
        if (!raw_ps) return 0;

        raw_ps_len = packet_ps->len;
        packet_free(packet_ps);
    }

    network_send(raw_ps, raw_ps_len);
    net_writable = 0;
    return 1;
}

int send_request_kill(PID pid) {
    static uint8_t *raw_kill = NULL;
    static uint16_t raw_kill_len = 0;

    /* Free memory from previous call */
    if (raw_kill) free(raw_kill);

    Packet *packet_kill = packet_create();
    if (!packet_kill) return 0;

    packet_kill->type = PKillPacket;
    packet_kill->kill_process.pid = (uint32_t)pid;
    raw_kill = packet_pack(packet_kill);
    if (!raw_kill) return 0;

    raw_kill_len = packet_kill->len;
    packet_free(packet_kill);

    network_send(raw_kill, raw_kill_len);
    net_writable = 0;
    return 1;
}
