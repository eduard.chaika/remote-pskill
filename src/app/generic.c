#include "generic.h"

static ReceiveBuffer recv_buf;

void receive_buf_reset(void) {
    recv_buf.head = 0;
    recv_buf.tail = 0;
    recv_buf.len = 0;
}

int receive_buf_store(uint8_t *data, uint16_t len) {
    if (len <= (RECV_BUF_SIZE - recv_buf.len)) {
        memcpy(recv_buf.data + recv_buf.tail, data, len);
        recv_buf.tail += len;
        recv_buf.len = recv_buf.tail - recv_buf.head;
        return 1;
    }
    /* There is no data in buffer */
    return 0;
}

uint16_t receive_buf_peek(uint8_t **data) {
    *data = recv_buf.data;
    return recv_buf.len;
}

void receive_buf_erase(uint16_t len) {
    if (len > recv_buf.len) {
        return;
    }

    recv_buf.head += len;
    recv_buf.len = recv_buf.tail - recv_buf.head;
    if (recv_buf.len) {
        memmove(recv_buf.data, recv_buf.data + recv_buf.head, recv_buf.len);
    }
    recv_buf.head = 0;
    recv_buf.tail = recv_buf.len;
}
