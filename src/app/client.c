#ifdef _WIN32
    #define _CRT_SECURE_NO_WARNINGS
#endif

#include <assert.h>
#include "network.h"
#include "system.h"
#include "protocol.h"
#include "generic.h"

#define reset_error()       SET_ERROR(_err, 0)

Error _err = { ._module = "client" };
volatile int net_writable = 0;
volatile AppState state = Disconnected;
volatile WritableCB writable_cb;
extern int __silent_log;

void usage_app(void) {
    #ifndef _WIN32
        const char *app_name = APP_NAME;
    #else
        const char *app_name = APP_NAME".exe";
    #endif
    
    printf("Usage:\n");
    printf("\t%s <host> <port> <log>\n", app_name);
    printf("\thost - server host address\n");
    printf("\tport - server port [1024..65535]\n");
    printf("\tlog - set '-q' for silent mode\n\n");
}

uint16_t scan_port(char *argv) {
    int port;
    int ret = sscanf(argv, "%d", &port);
    if (!ret || port < 1024 || port > 65535) {
        LOGE(_err, "Invalid port!\n");
        return 0;
    }
    return (uint16_t)port;
}

int scan_args(int argc, char *argv[], char *host, uint16_t *port) {
    size_t host_len;

    if (argc < 3) {
        LOGE(_err, "Too few arguments!\n");
        return 0;
    }

    host_len = strlen(argv[1]);
    if (host_len > NET_ADDRLEN - 1) {
        LOGE(_err, "Too long host address!\n");
        return 0;
    }
    memcpy(host, argv[1], host_len);
    host[host_len] = '\0';

    *port = scan_port(argv[2]);
    if (!*port) {
        return 0;
    }

   if (argc >= 4) {
        size_t silent_len = strlen("-q");
        int is_silent = (strlen(argv[3]) == silent_len) & (!strncmp(argv[3], "-q", silent_len));
        if (is_silent) {
            __silent_log = 1;
        }
        else {
            LOGE(_err, "Invalid argument!\n");
            return 0;
        }
    }

    return 1;
}

/* Network callbacks */
void on_connect(void *arg);
void on_writable(void *arg);
void on_receive(void *arg);
void on_disconnect(void *arg);
void on_error(void *arg);

/* Client dependent functions */
int read_packet(Packet **packet);
int send_handshake(void);
int send_response(const char *resp);
int send_process_list(void *ps);

int main(int argc, char *argv[]) {
    reset_error();

    int ret = 0;
    NetworkParam param;

    if (!scan_args(argc, argv, param.host, &param.port)) {
        usage_app();
        return 1;
    }

    /* Network initialization */
    param.mode = NetworkCient;
    param.cb.on_connect = &on_connect;
    param.cb.on_writable = &on_writable;
    param.cb.on_receive = &on_receive;
    param.cb.on_disconnect = &on_disconnect;
    param.cb.on_error = &on_error;

    if (!network_init(&param)) {
        network_perror();
        goto exit;
    }

    for(;;) {
        if (!network_poll()) {
            network_perror();
            goto exit;
        }

        switch(state) {
            case Disconnected:
            break;
            case Handshake:
                if (!send_handshake()) {
                    system_perror();
                    goto exit;
                }
                state = HandshakeWait;
            break;
            case HandshakeWait: {
                Packet *packet;
                if ((ret = read_packet(&packet)) == 1) {
                    if (packet->type == ServerHSPacket && packet->handshake.id == PACKET_HANDSHAKE) {
                        LOGD(_err, "Server accepted handshake!\n");
                        state = Established;
                    }
                    else {
                        LOGD(_err, "Server sent a wrong handshake!\n");
                        network_disconnect();
                    }
                    packet_free_all(packet);
                }
                else if (ret < 0) goto exit;
            }
            break;
            case Established: {
                Packet *packet;
                if ((ret = read_packet(&packet)) == 1) {
                    switch(packet->type) {
                        case GetPLPacket: {
                            LOGD(_err, "Getting process list\n");
                            char *ps;
                            if (!get_process_list(&ps)) {
                                if (!send_response(system_perror())) {
                                    goto exit;
                                }
                            }
                            else {
                                if (!send_process_list(ps)) {
                                    system_perror();
                                    goto exit;
                                }
                            }
                        }
                        break;
                        case PKillPacket:
                            LOGD(_err, "Killing process with pid %u\n", packet->kill_process.pid);
                            process_kill((PID)packet->kill_process.pid);
                            if (!send_response(system_perror())) {
                                goto exit;
                            }
                        break;
                    }

                    packet_free_all(packet);
                }
                else if (ret < 0) goto exit;
            }
            break;
        }

        system_sleep(1);
    }

exit:
    network_deinit();
    return 1;
    /* I don't free any allocated memory, it doesn't matter here */
}

void on_connect(void *arg) {
    LOGM(_err, "Connected to server!\n");
    receive_buf_reset();
    state = Handshake;
}

void on_writable(void *arg) {
    net_writable = 1;
    if (writable_cb) {
        if (!writable_cb(NULL)) {
            system_perror();
            exit(1);
        }
    }
}

void on_receive(void *arg) {
    /* After exiting the function, the data in the buffer may be lost */
    NetworkBuffer *received = (NetworkBuffer*)arg;
    LOGD(_err, "Received %d bytes from server!\n", received->len);
    receive_buf_store(received->data, received->len);
}

void on_disconnect(void *arg) {
    LOGM(_err, "Disconnected from server!\n");
    state = Disconnected;
}

void on_error(void *arg) {
    network_perror();
    exit(1);
}

int send_handshake(void) {
    static uint8_t *raw_hs = NULL;
    static uint16_t raw_hs_len = 0;

    if (!raw_hs) {
        Packet *packet_hs = packet_create();
        if (!packet_hs) return 0;

        packet_hs->type = ClientHSPacket;
        raw_hs = packet_pack(packet_hs);
        if (!raw_hs) return 0;

        raw_hs_len = packet_hs->len;
        packet_free(packet_hs);
    }

    network_send(raw_hs, raw_hs_len);
    net_writable = 0;
    return 1;
}

int read_packet(Packet **packet) {
    uint8_t *data;
    uint16_t len = receive_buf_peek(&data);
    *packet = packet_unpack(data, len);
    if (!*packet) {
        if (protocol_gerror() != PROTOCOL_ERR_BAD_PACKET) {
            protocol_perror();
            return -1;
        }
        return 0;
    }
    
    receive_buf_erase((*packet)->len);
    return 1;
}

int send_response(const char *resp) {
    static uint8_t *raw_resp = NULL;
    static uint16_t raw_resp_len = 0;   

    if (raw_resp) {
        free(raw_resp);    /* Free memory from previous call */
    }

    Packet *packet_resp = packet_create();
    if (!packet_resp) return 0;

    packet_resp->type = ResponsePacket;
    packet_resp->response.data = (char*)resp;
    raw_resp = packet_pack(packet_resp);
    if (!raw_resp) return 0;

    raw_resp_len = packet_resp->len;
    packet_free(packet_resp);

    network_send(raw_resp, raw_resp_len);
    net_writable = 0;
    return 1;
}

int send_process_list(void *ps) {
    static char *_ps = NULL;
    static size_t _ps_size = 0;
    static uint8_t *raw_ps = NULL;
    static uint16_t raw_ps_len = 0;
    static uint8_t next_packet = NullPacket;
    
    /* We'll save data from first call */
    if (ps) {
        _ps = ps;
        _ps_size = strlen(_ps);
        next_packet = PLBeginPacket;
    }

    /* Free memory from previous call */
    if (raw_ps) free(raw_ps);   

    Packet *packet_ps = packet_create();
    if (!packet_ps) return 0;

    size_t send_size = MIN(_ps_size, PACKET_MAXDATALEN);
    char _tmp = _ps[send_size];
    _ps[send_size] = '\0';

    packet_ps->type = next_packet;
    packet_ps->process_list.data = _ps;
    raw_ps = packet_pack(packet_ps);
    if (!raw_ps) return 0;

    _ps[send_size] = _tmp;
    _ps += send_size;
    _ps_size -= send_size;

    if (next_packet != PLEndPacket) {
        next_packet = (_ps_size > PACKET_MAXDATALEN) ? PLChunkPacket : PLEndPacket;
        writable_cb = send_process_list;
    }
    else {
        next_packet = NullPacket;
        writable_cb = NULL;
    }
    
    raw_ps_len = packet_ps->len;
    packet_free(packet_ps);
    
    network_send(raw_ps, raw_ps_len);
    net_writable = 0;
    return 1;
}
