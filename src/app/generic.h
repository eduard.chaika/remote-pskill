#ifndef __GENERIC_H__
#define __GENERIC_H__

#include "protocol.h"

#define MIN(x,y)            ((x)<(y) ? (x):(y))
#define RECV_BUF_SIZE       (PACKET_MAXLEN * 4)

typedef int(*WritableCB)(void*);

typedef enum {
    Disconnected,
    HandshakeWait,
    Handshake,
    Established
} AppState;

typedef struct {
    uint16_t head;
    uint16_t tail;
    uint16_t len;
    uint8_t data[RECV_BUF_SIZE];
} ReceiveBuffer;

int receive_buf_store(uint8_t *data, uint16_t len);

uint16_t receive_buf_peek(uint8_t **data);

void receive_buf_erase(uint16_t len);

void receive_buf_reset(void);

#endif /* __GENERIC_H__ */
