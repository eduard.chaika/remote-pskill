#ifndef __COMMON_LOG_H__
#define __COMMON_LOG_H__

#include <stdio.h>

#ifndef APP_NAME
#define APP_NAME ""
#endif

int __silent_log;

typedef struct {
    const char *_module;
    const char *_func;
    int _line;
    int _code;
} Error;

#define SET_ERROR(err, code) \
                do { \
                    err._func = __FUNCTION__; \
                    err._line = __LINE__; \
                    err._code = code; \
                } while(0)

#define LOGM_RELEASE(fmt, ...) \
                do { \
                    if (!__silent_log) { \
                        fprintf(stdout, APP_NAME ": (Info) " fmt, ##__VA_ARGS__); \
                    } \
                } while(0);

#define LOGE_RELEASE(fmt, ...) \
                do { \
                    fprintf(stderr, APP_NAME ": (Error) " fmt, ##__VA_ARGS__); \
                } while(0);


#define LOG_DEBUG(type, err, fmt, ...) \
                do { \
                    if (!__silent_log) { \
                        fprintf(stdout, "(%s) " APP_NAME " (%s: %s-%d): " fmt, type, err._module, __FUNCTION__, __LINE__, ##__VA_ARGS__); \
                    } \
                } while(0);

#define LOGE_DEBUG(err, fmt, ...) \
                do { \
                    fprintf(stderr, "(Error) " APP_NAME " (%s: %s-%d): " fmt, err._module, err._func, err._line, ##__VA_ARGS__); \
                } while(0);

#ifdef DEBUG_LOG
    #define LOGM(err, fmt, ...)     LOG_DEBUG("Info", err, fmt, ##__VA_ARGS__)
    #define LOGE(err, fmt, ...)     LOGE_DEBUG(err, fmt, ##__VA_ARGS__)
    #define LOGD(err, fmt, ...)     LOG_DEBUG("Debug", err, fmt, ##__VA_ARGS__)
#else
    #define LOGM(err, fmt, ...)     LOGM_RELEASE(fmt, ##__VA_ARGS__)
    #define LOGE(err, fmt, ...)     LOGE_RELEASE(fmt, ##__VA_ARGS__)
    #define LOGD(err, fmt, ...)
#endif

#endif /* __COMMON_LOG_H__ */
