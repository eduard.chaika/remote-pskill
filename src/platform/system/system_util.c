#include "system_util.h"

void buffer_reset(_Buffer *buf) {
    if (buf->data && !buf->curr) {
        buf->data[buf->curr] = '\0';
    }
    buf->curr = 0; 
}

int buffer_alloc(_Buffer *buf, size_t data_len) {
    if (buf->len == 0) {
        buf->len = (data_len / SYSTEM_BUF_ALLOC_LEN + 1) * SYSTEM_BUF_ALLOC_LEN;
        buf->data = (char*)malloc(buf->len);
        if (!buf->data) return 0;
    }

    if (((buf->len - buf->curr) - 1) < data_len) {
        buf->len += (data_len / SYSTEM_BUF_ALLOC_LEN + 1) * SYSTEM_BUF_ALLOC_LEN;
        buf->data = (char*)realloc(buf->data, buf->len);
        if (!buf->data) return 0;
    }

    return 1;
}

int buffer_append(_Buffer *buf, char *data) {
    size_t data_len = (uint32_t)strlen(data);
    if (!buffer_alloc(buf, data_len)) return 0;

    memcpy(&buf->data[buf->curr], data, data_len);
    buf->curr += data_len;
    buf->data[buf->curr] = '\0';    

    return 1;
}

int buffer_append_ch(_Buffer *buf, char ch) {
    size_t data_len = sizeof(ch);
    if (!buffer_alloc(buf, data_len)) return 0;

    buf->data[buf->curr++] = ch;
    buf->data[buf->curr] = '\0';

    return 1;  
}

void buffer_backspace(_Buffer *buf) {
    if (buf->curr > 0) {
        buf->data[--buf->curr] = '\0';
    }
}

void buffer_free(_Buffer *buf) {
    if (buf) {
        buf->curr = 0;
        buf->len = 0;
        free(buf->data);
    }
}
