#ifndef __PLATFORM_SYSTEM_UTIL_H__
#define __PLATFORM_SYSTEM_UTIL_H__

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define SYSTEM_BUF_ALLOC_LEN    (512)

typedef struct {
    size_t curr;
    size_t len;
    char *data;
} _Buffer;

void buffer_reset(_Buffer *buf);

int buffer_alloc(_Buffer *buf, size_t data_len);

int buffer_append(_Buffer *buf, char *data);

int buffer_append_ch(_Buffer *buf, char ch);

void buffer_backspace(_Buffer *buf);

void buffer_free(_Buffer *buf);

#endif /* __PLATFORM_SYSTEM_UTIL_H__ */
