set(NAME system)
set(SRC ${NAME} "${NAME}_util")

add_library(${NAME} OBJECT ${SRC})

target_include_directories(${NAME} PUBLIC ${CMAKE_SOURCE_DIR}/src/common)
