#ifndef __PLATFORM_SYSTEM_H__
#define __PLATFORM_SYSTEM_H__

#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "log.h"
#include "system_util.h"

#ifdef __linux__
    #include <dirent.h>
    #include <fcntl.h>
    #include <unistd.h>
    #include <sys/types.h>
    #include <signal.h>
    #include <errno.h>
#elif _WIN32
    #include <windows.h>
    #include <tlhelp32.h>
    #include <tchar.h>
    #include <conio.h>
#else
    #error "Platform is not supported!"
#endif

#define SYSTEM_SUCCEEDED        (0x00000000)
#define SYSTEM_ERR_MEM_ALLOC    (0xE0000000)

#ifdef _WIN32
    typedef unsigned long PID;
#else
    typedef pid_t PID;
#endif

int get_process_list(char **result);

int process_kill(PID pid);

int read_console_line(char **result);

void system_sleep(int milliseconds);

const int system_gerror(void);

const char *system_perror(void);

#endif /* __PLATFORM_SYSTEM_H__ */
