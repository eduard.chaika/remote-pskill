#include "system.h"

#define BUFFER_ERROR_LEN    (128)
#define set_error(code)     SET_ERROR(_err, code)
#define reset_error()       SET_ERROR(_err, SYSTEM_SUCCEEDED)

static Error _err = { ._module = "platform-system" };
static _Buffer _inbuf, _psbuf;

#ifdef _WIN32
static inline const char *platform_perror(void) {
    static char error_str[BUFFER_ERROR_LEN];
    DWORD flags = FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS;
    FormatMessageA(flags, NULL, _err._code, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)error_str, BUFFER_ERROR_LEN, NULL);
    LOGE(_err, "(%d) %s\n", _err._code, error_str);
    return error_str;
}
#else
static inline const char *platform_perror(void) {
    LOGE(_err, "(%d) %s\n", _err._code, strerror(_err._code));
    return strerror(_err._code);
}

static int is_pid(char *pid) {
    /**
     * Actually, the function checks if a string contains only numbers
     * If not, this is not a pid
    */
    size_t len = strlen(pid);
    while(len--) {
        if (*pid < '0' || *pid > '9') return 0;
        pid++;
    }
    return 1;
}

static char *get_exename(char *exename) {
    /**
     * The function makes 'example' from '/home/root/example'
     * The return value must be saved and freed
    */
    reset_error();

    char *_exename = NULL;
    size_t len = strlen(exename);
    exename += len - 1;

    while(len--) {
        if (*exename == '/') break;
        exename--;
    }

    len = strlen(exename + 1);
    _exename = (char*)malloc(len + 1);
    if (!_exename) {
        set_error(SYSTEM_ERR_MEM_ALLOC);
        return NULL;
    }

    snprintf(_exename, len + 1, "%s", exename + 1);
    return _exename;
}
#endif

int get_process_list(char **result) {
    reset_error();
    buffer_reset(&_psbuf);

    static char buf[512];

    #ifdef _WIN32
        HANDLE snapshot;
        PROCESSENTRY32 p_entry;

        snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
        if(snapshot == INVALID_HANDLE_VALUE) {
            set_error(GetLastError());
            return 0;
        }

        p_entry.dwSize = sizeof(PROCESSENTRY32);

        if(!Process32First(snapshot, &p_entry)) {
            set_error(GetLastError());
            CloseHandle(snapshot);
            return 0;
        }

        snprintf(buf, sizeof(buf), "%10s\tNAME\n", "PID");
        if (!buffer_append(&_psbuf, buf)) {
            set_error(SYSTEM_ERR_MEM_ALLOC);
            return 0;
        }
        do {
            snprintf(buf, sizeof(buf), "%*lu\t%s\n", 10, p_entry.th32ProcessID, p_entry.szExeFile);
            if (!buffer_append(&_psbuf, buf)) {
                set_error(SYSTEM_ERR_MEM_ALLOC);
                return 0;
            }
        } while(Process32Next(snapshot, &p_entry));

        CloseHandle(snapshot);
    #else
        DIR *dir;
        struct dirent *d_entry;

        if (!(dir = opendir("/proc"))) {
            set_error(errno);
            return 0;
        }

        snprintf(buf, sizeof(buf), "%10s\tNAME\n", "PID");
        if (!buffer_append(&_psbuf, buf)) {
            set_error(SYSTEM_ERR_MEM_ALLOC);
            return 0;
        }

        while ((d_entry = readdir(dir)) != NULL) {
            if (is_pid(d_entry->d_name)) {
                char *exename = NULL;       
                ssize_t readlink_len = 0;

                snprintf(buf, sizeof(buf), "/proc/%s/exe", d_entry->d_name);
                if ((readlink_len = readlink(buf, buf, sizeof(buf) - 1)) < 0) {     
                    if (errno == EACCES) {
                        /* We'll get EACCES error if program running without sudo */
                        snprintf(buf, sizeof(buf), "[Permission denied]");
                        readlink_len = strlen(buf);
                    }
                    else {
                        set_error(errno);
                        return 0;
                    }         
                }
                /* readlink() does not append a null byte to buf */
                buf[readlink_len] = '\0';

                if (!(exename = get_exename(buf))) return 0;

                snprintf(buf, sizeof(buf), "%10s\t%s\n", d_entry->d_name, exename);
                if (!buffer_append(&_psbuf, buf)) {
                    set_error(SYSTEM_ERR_MEM_ALLOC);
                    return 0;
                }
                free(exename);
            }
        }

        closedir(dir);
    #endif

    *result = _psbuf.data;
    return 1;
}

int process_kill(PID pid) {
    reset_error();

    #ifdef _WIN32
        HANDLE process_hdl;
        if (!(process_hdl = OpenProcess(PROCESS_ALL_ACCESS, 0, pid))) {
            set_error(GetLastError());
            return 0;
        }

        if (TerminateProcess(process_hdl, 0) == 0) {
            set_error(GetLastError());
            return 0;
        }
    #else
        if (kill(pid, SIGKILL) != 0) {
            set_error(errno);
            return 0;
        }
    #endif

    return 1;
}

int read_console_line(char **result) {
    reset_error();

    char ch;

    #ifndef _WIN32
        static int stdin_inited = 0;
        if (!stdin_inited) {
            if (fcntl(STDIN_FILENO, F_SETFL, fcntl(STDIN_FILENO, F_GETFL) | O_NONBLOCK) < 0) {
                set_error(errno);
                return -1;
            }
            stdin_inited = 1;
        }
    #endif

    #ifdef _WIN32
        while(_kbhit()) {
            if((ch = _getch()) == -32) {
                _getch();
                continue;
            }
            _putch(ch);

    #else
        while(read(STDIN_FILENO, &ch, sizeof(ch)) > 0) {
    #endif
            if (ch == '\b') {
                #ifdef _WIN32
                    printf(" \b");
                #endif
                buffer_backspace(&_inbuf);
            }
            else {
                if (ch == '\r' || ch == '\n') {
                    buffer_reset(&_inbuf);
                    *result = _inbuf.data;
                    #ifdef _WIN32
                        printf("%s\n", *result);
                    #endif
                    return (int)strlen(_inbuf.data);
                }
                if (!buffer_append_ch(&_inbuf, ch)) {
                    set_error(SYSTEM_ERR_MEM_ALLOC);
                    return -1;
                }
            }
        }

    return 0;
}

void system_sleep(int milliseconds) {
    #ifdef _WIN32
        Sleep((DWORD)milliseconds);
    #else
        usleep(milliseconds * 1000);
    #endif
}

inline const int system_gerror(void) {
    return _err._code;
}

const char *system_perror(void) {
    switch(_err._code) {
        case SYSTEM_SUCCEEDED: {
            const char *success = "Operation succeeded!";
            LOGD(_err, "%s\n", success);
            return success;
        }
        break;
        case SYSTEM_ERR_MEM_ALLOC: {
            const char *failed = "Memory allocation error!";
            LOGE(_err, "%s\n", failed);
            return failed;
        }
        break;
        default:
            return platform_perror();
        break;
    }
}
