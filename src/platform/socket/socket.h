#ifndef __PLATFORM_SOCKET_H__
#define __PLATFORM_SOCKET_H__

#include <stdio.h>
#include <stdint.h>
#include "log.h"

#ifdef __linux__
    #include <string.h>
    #include <unistd.h>
    #include <fcntl.h>
    #include <signal.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <netdb.h>
    #include <errno.h>
#elif _WIN32
    #include <winsock2.h>
    #include <ws2tcpip.h>
    #include <windows.h>
#else
    #error "Platform is not supported!"
#endif

#define SOCKET_SUCCEEDED                        (0x00000000)
#define SOCKET_ERR_CONN_CLOSED                  (0xE0000001)
#ifdef _WIN32
    #define SOCKET_ERR_TRY_AGAIN                (WSAEWOULDBLOCK)
    #define SOCKET_ERR_CONN_REFUSED             (WSAECONNREFUSED)
    #define SOCKET_ERR_CONN_ABORT               (WSAECONNABORTED)
    #define SOCKET_ERR_CONN_RESET               (WSAECONNRESET)
#else
    /**
     *  POSIX.1 allows either errors EWOULDBLOCK and EAGAIN
     *  to be returned for this case, and does not require these
     *  constants to have the same value, so a portable application
     *  should check for both possibilities. So I have to take care of it.
     */
    #define SOCKET_ERR_TRY_AGAIN                (EWOULDBLOCK)
    #define SOCKET_ERR_CONN_REFUSED             (ECONNREFUSED)
    #define SOCKET_ERR_CONN_ABORT               (ECONNABORTED)
    #define SOCKET_ERR_CONN_RESET               (ECONNRESET)
#endif

#ifdef _WIN32
    #define socket_errno()                      WSAGetLastError()
#else
    typedef int                                 SOCKET;
    #define INVALID_SOCKET                      (-1)
    #define SOCKET_ERROR                        (-1)
    #define ZeroMemory(dest,len)                memset((dest),0,(len))
    #define SD_RECEIVE                          SHUT_RD     // Shutdown receive operations
    #define SD_SEND                             SHUT_WR     // Shutdown send operations
    #define SD_BOTH                             SHUT_RDWR   // Shutdown both send and receive operations
    #define socket_errno()                      errno
#endif

int socket_init(void);

SOCKET socket_create(int domain, int type, int protocol);

SOCKET socket_accept(SOCKET sock, struct sockaddr *addr, socklen_t *addrlen);

int socket_addrinfo(const char *node, const char *service, const struct addrinfo *hints, struct addrinfo **res);

int socket_connect(SOCKET sock, const struct sockaddr *addr, socklen_t addrlen);

int socket_bind(SOCKET sock, const struct sockaddr *addr, socklen_t addrlen);

int socket_listen(SOCKET sock, int backlog);

int socket_send(SOCKET sock, const void *buf, int len, int flags);

int socket_read(SOCKET sock, void *buf, int len, int flags);

int socket_shutdown(SOCKET sock, int how);

int socket_setnonblock(SOCKET sock, int blocking);

int socket_reuseaddr(SOCKET sock);

void socket_freeaddr(struct addrinfo *ai);

void socket_close(SOCKET sock);

void socket_deinit(void);

const int socket_gerror(void);

const char *socket_perror(void);

#endif /* __PLATFORM_SOCKET_H__ */
