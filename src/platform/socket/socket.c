#include "socket.h"

#ifdef _WIN32
    #pragma comment (lib, "Ws2_32.lib")
    #pragma comment (lib, "Mswsock.lib")
    #pragma comment (lib, "AdvApi32.lib")
#endif

#define BUFFER_ERROR_LEN    (128)
#define set_error(code)     SET_ERROR(_err, code)
#define reset_error()       SET_ERROR(_err, SOCKET_SUCCEEDED)

static int _sock_nonblock;
static Error _err = { ._module = "platform-socket" };

int socket_init(void) {
    reset_error();

    #ifdef _WIN32
        int ret;
        WSADATA wsa;
        if ((ret = WSAStartup(MAKEWORD(2,2), &wsa)) != 0) {
            set_error(ret);
            return 0;
        }
    #else
        signal(SIGPIPE, SIG_IGN);
    #endif

    return 1;
}

SOCKET socket_create(int domain, int type, int protocol) {
    reset_error();

    SOCKET _socket = socket(domain, type, protocol);
    if (_socket == INVALID_SOCKET) {
        set_error(socket_errno());
    }

    return _socket;
}

SOCKET socket_accept(SOCKET sock, struct sockaddr *addr, socklen_t *addrlen) {
    reset_error();

    #ifdef _WIN32
        SOCKET _socket = accept(sock, addr, (int*)addrlen);
    #else
        SOCKET _socket = accept(sock, addr, addrlen);
    #endif
    
    if (_socket == INVALID_SOCKET) {
        set_error(socket_errno());
    }

    return _socket;    
}

int socket_addrinfo(const char *node, const char *service, const struct addrinfo *hints, struct addrinfo **res) {
    reset_error();

    if (getaddrinfo(node, service, hints, res) != SOCKET_SUCCEEDED) {
        set_error(socket_errno());
        return 0;
    }

    return 1;
}

int socket_connect(SOCKET sock, const struct sockaddr *addr, socklen_t addrlen) {
    reset_error();

    if (connect(sock, addr, addrlen) != SOCKET_SUCCEEDED) {
        set_error(socket_errno());
        return 0;
    }

    return 1;
}

int socket_bind(SOCKET sock, const struct sockaddr *addr, socklen_t addrlen) {
    reset_error();

    if (bind(sock, addr, addrlen) != SOCKET_SUCCEEDED) {
        set_error(socket_errno());
        return 0;
    }

    return 1;
}

int socket_listen(SOCKET sock, int backlog) {
    reset_error();

    if (listen(sock, backlog) != SOCKET_SUCCEEDED) {
        set_error(socket_errno());
        return 0;
    }

    return 1;
}

int socket_send(SOCKET sock, const void *buf, int len, int flags) {
    reset_error();

    int ret;

    #ifdef _WIN32
        if ((ret = send(sock, buf, len, flags)) == SOCKET_ERROR) {
    #else
        if ((ret = send(sock, buf, (size_t)len, flags)) == SOCKET_ERROR) {
    #endif

        switch(socket_errno()) {
            #ifdef __linux__
            case EPIPE:
            #endif
            case SOCKET_ERR_CONN_ABORT:
            case SOCKET_ERR_CONN_RESET:
                set_error(SOCKET_ERR_CONN_CLOSED);
            break;
            default:
                set_error(socket_errno());
            break;
        }

        return SOCKET_ERROR;
    }

    return ret;
}

int socket_read(SOCKET sock, void *buf, int len, int flags) {
    reset_error();

    #ifdef _WIN32
        int ret = recv(sock, buf, len, flags);
    #else
        int ret = recv(sock, buf, (size_t)len, flags);
    #endif
        if (ret < 0) {
            switch(socket_errno()) {
                case SOCKET_ERR_CONN_ABORT:
                case SOCKET_ERR_CONN_RESET:
                    set_error(SOCKET_ERR_CONN_CLOSED);
                break;
                default:
                    set_error(socket_errno());
                break;
            }
            return 0;
        }
        else if (ret == 0 && _sock_nonblock) {
            set_error(SOCKET_ERR_CONN_CLOSED);
            return 0;
        }

    return ret;
}

int socket_shutdown(SOCKET sock, int how) {
    reset_error();

    if (shutdown(sock, how) != SOCKET_SUCCEEDED) {
        set_error(socket_errno());
        return 0;
    }

    return 1;
}

int socket_setnonblock(SOCKET sock, int blocking) {
    reset_error();

    #ifdef _WIN32
        int ret;
        _sock_nonblock = blocking;
    
        if ((ret = ioctlsocket(sock, FIONBIO, (u_long*)&blocking)) != 0) {
            set_error(socket_errno());
            return 0;
        }

        return 1;
    #else
        int flags = fcntl(sock, F_GETFL, 0);
        if (flags == -1) {
            set_error(socket_errno());
            return 0;
        }
        flags = blocking ? (flags | O_NONBLOCK) : (flags & ~O_NONBLOCK);
        _sock_nonblock = blocking;

        if (fcntl(sock, F_SETFL, flags) == -1) {
            set_error(socket_errno());
            return 0;
        }

        return 1;
    #endif
}

int socket_reuseaddr(SOCKET sock) {
    reset_error();

    #ifdef _WIN32
        if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (const char*)&(int){1}, sizeof(int)) < 0) {
    #else
        if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (const void*)&(int){1}, sizeof(int)) < 0) {
    #endif
        set_error(socket_errno());
        return 0;
    }
    
    return 1;
}

void socket_freeaddr(struct addrinfo *ai) {
    freeaddrinfo(ai);
}

void socket_close(SOCKET sock) {
    #ifdef _WIN32
        closesocket(sock);
    #else
        close(sock);
    #endif
}

void socket_deinit(void) {
    #ifdef _WIN32
        WSACleanup();
    #endif
}

const int socket_gerror(void) {
    return _err._code;
}

const char *socket_perror(void) {
    #ifdef _WIN32
    switch (_err._code) {
        case WSASYSNOTREADY:
        case WSAVERNOTSUPPORTED:
        case WSAEINPROGRESS:
        case WSAEPROCLIM:
        case WSAEFAULT: {
            const char *failed = "Windows Sockets subsystem failed!";
            LOGE(_err, "(%d) %s", _err._code, failed);
            return failed;
        }
        break;
        default: {
            static char error_str[BUFFER_ERROR_LEN];
            DWORD flags = FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS;
            FormatMessageA(flags, NULL, _err._code, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)error_str, BUFFER_ERROR_LEN, NULL);
            LOGE(_err, "(%d) %s\n", _err._code, error_str);
            return error_str;
        } break;
    }
    #else
    LOGE(_err, "(%d) %s\n", _err._code, strerror(_err._code));
    return strerror(_err._code);
    #endif
}
