#include "network.h"

#define set_error(code)     SET_ERROR(_err, code)
#define reset_error()       SET_ERROR(_err, NET_SUCCEEDED)

typedef enum {
    Default,
    Disconnected,
    Listening,
    Connected
} NetworkState;

typedef struct {
    SOCKET sock;
    SOCKET listensock;
    NetworkState state;
    NetworkParam param;
    NetworkBuffer recv_buf;
    NetworkBuffer send_buf;
} NetworkCtx;

static Error _err = { ._module = "network" };
static uint8_t recv_buf[NET_BUFFER_SIZE];
static NetworkCtx net = {
    .sock = INVALID_SOCKET,
    .listensock = INVALID_SOCKET,
    .state = Default,
    .recv_buf = { .data = recv_buf, .len = 0},
    .send_buf = { .data = NULL, .len = 0},
};

static int network_listen(void) {
    reset_error();

    char _port[8];
    struct addrinfo *result, hints;

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;
    snprintf(_port, sizeof(_port), "%u", net.param.port);

    if (!socket_addrinfo(NULL, _port, &hints, &result)) {
        goto socket_error;
    }

    net.listensock = socket_create(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (net.listensock == INVALID_SOCKET) {
        goto socket_error;
    }

    if (!socket_reuseaddr(net.listensock)) {
        goto socket_error;
    }

    if (!socket_setnonblock(net.listensock, 1)) {
        goto socket_error;
    }

    if (!socket_bind(net.listensock, result->ai_addr, (int)result->ai_addrlen)) {
        goto socket_error;
    }

    socket_freeaddr(result);
    result = NULL;

    if (!socket_listen(net.listensock, SOMAXCONN)) {
        goto socket_error;
    }

    return 1;

socket_error:
    set_error(NET_ERR_SOCKET_FAIL);
    if (result) socket_freeaddr(result);
    socket_close(net.listensock);
    return 0;
}

static int network_accept(void) {
    net.sock = socket_accept(net.listensock, NULL, NULL);
    if (net.sock == INVALID_SOCKET) {
        if (socket_gerror() == SOCKET_ERR_TRY_AGAIN) {
            return 0;   /* No clients to accept */
        }
        goto error;
    }

    if (!socket_setnonblock(net.sock, 1)) {
        socket_close(net.sock);
        goto error;
    }

    socket_close(net.listensock);
    return 1;

error:
    set_error(NET_ERR_SOCKET_FAIL);
    socket_close(net.listensock);
    return -1;
}

static int network_connect(void) {
    reset_error();

    char _port[8];
    struct addrinfo *result, *_result, hints;

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    snprintf(_port, sizeof(_port), "%u", net.param.port);
    
    result = NULL;
    if (!socket_addrinfo(net.param.host, _port, &hints, &result)) {
        goto socket_error;
    }

    for(_result = result; _result != NULL; _result = _result->ai_next) {
        net.sock = socket_create(_result->ai_family, _result->ai_socktype, _result->ai_protocol);
        if (net.sock == INVALID_SOCKET) {
            goto socket_error;
        }

        if (!socket_connect(net.sock, _result->ai_addr, (int)_result->ai_addrlen)) {
            socket_close(net.sock);
            net.sock = INVALID_SOCKET;
            continue;
        }

        break;
    }

    socket_freeaddr(result);
    result = NULL;

    if (net.sock == INVALID_SOCKET) {
        if (socket_gerror() == SOCKET_ERR_CONN_REFUSED) {
            return 0;   /* No server to connect */
        }
        goto socket_error;
    }

    if (!socket_setnonblock(net.sock, 1)) {
        socket_close(net.sock);
        goto socket_error;
    }

    return 1;

socket_error:
    set_error(NET_ERR_SOCKET_FAIL);
    if (result) socket_freeaddr(result);
    return -1;
}

static int network_sendbuf(void) {
    reset_error();

    int ret;

    if ((ret = socket_send(net.sock, net.send_buf.data, net.send_buf.len, 0)) < 0) {
        if (socket_gerror() == SOCKET_ERR_CONN_CLOSED) {
            set_error(NET_ERR_CONN_CLOSED);
            return -1;
        }
        set_error(NET_ERR_SOCKET_FAIL);
        return -1;
    }

    return ret;
}

static int network_receive(uint8_t *data, uint16_t len) {
    reset_error();

    int ret;

    if (!(ret = socket_read(net.sock, data, len, 0))) {
        switch(socket_gerror()) {
            case SOCKET_ERR_CONN_CLOSED:
                set_error(NET_ERR_CONN_CLOSED);
                return -1;
            break;
            case SOCKET_ERR_TRY_AGAIN:
                return 0;
            break;
            default:
                set_error(NET_ERR_SOCKET_FAIL);
                return -1;
            break;
        }
    } 

    return ret;
}

int network_init(NetworkParam *param) {
    reset_error();

    if (param->mode >= NetworkNull) {
        set_error(NET_ERR_INVALID_ARG);
        return 0;
    }

    if (!socket_init()) {
        set_error(NET_ERR_SOCKET_FAIL);
        return 0; 
    }

    net.state = Disconnected;
    net.param = *param;
    return 1;
}

int network_poll(void) {
    reset_error();

    int ret;

    switch(net.state) {
        case Default:
            set_error(NET_ERR_NOT_PERMITTED);
            return 0;
        break;
        case Disconnected:
            if (net.param.mode == NetworkServer) {
                if ((ret = network_listen()) > 0) {
                    net.state = Listening;
                }
                else if (ret < 0) goto error;
            }
            else if (net.param.mode == NetworkCient) {
                if ((ret = network_connect()) > 0) {
                    net.state = Connected;
                    if (net.param.cb.on_connect) net.param.cb.on_connect(NULL);
                    if (net.param.cb.on_writable) net.param.cb.on_writable(NULL);
                }
                else if (ret < 0) goto error;
            }
        break;
        case Listening:
            if (net.param.mode == NetworkServer) {
                if ((ret = network_accept()) > 0) {
                    net.state = Connected;
                    if (net.param.cb.on_connect) net.param.cb.on_connect(NULL);
                    if (net.param.cb.on_writable) net.param.cb.on_writable(NULL);
                }
                else if (ret < 0) goto error;
            }
        break;
        case Connected:
            if ((ret = network_receive(net.recv_buf.data, NET_BUFFER_SIZE)) > 0) {
                net.recv_buf.len = ret;
                if (net.param.cb.on_receive) net.param.cb.on_receive((void*)&net.recv_buf);
            }
            else if (ret < 0) {
                if (network_gerror() == NET_ERR_CONN_CLOSED) {
                    network_disconnect();
                    break;
                }
                else goto error;
            }
            
            if (net.send_buf.len && (ret = network_sendbuf()) > 0) {
                net.send_buf.len -= ret;
                net.send_buf.data += ret;
                if (!net.send_buf.len) {
                    /**
                     *  *Nonblocking*
                     *  The number of bytes written can be between 1 and the requested length 
                     *  So need to call back when all the data is actually sent
                    */
                    if (net.param.cb.on_writable) net.param.cb.on_writable(NULL);
                }                
            }
            else if (ret < 0) {
                if (network_gerror() == NET_ERR_CONN_CLOSED) {
                    network_disconnect();
                }
                else goto error;
            }
        break;
    }

    return 1;

error:
    if (net.param.cb.on_error) net.param.cb.on_error(NULL);
    return 0;
}

int network_send(uint8_t *data, uint16_t len) {
    net.send_buf.data = data;
    net.send_buf.len = len;
    return 1;
}

int network_disconnect(void) {
    reset_error();

    if (net.state == Disconnected) {
        return 1;
    }

    #if 0
    if (!socket_shutdown(sock, SD_BOTH)) {
        set_error(NET_ERR_SOCKET_FAIL);
        return 0;
    }
    #endif

    socket_close(net.sock);
    net.state = Disconnected;
    if (net.param.cb.on_disconnect) net.param.cb.on_disconnect(NULL);
    return 1;
}

void network_deinit(void) {
    socket_deinit();
}

const int network_gerror(void) {
    return (_err._code != NET_ERR_SOCKET_FAIL) ? _err._code : socket_gerror();
}

const char *network_perror(void) {
    switch(_err._code) {
        case NET_ERR_SOCKET_FAIL:
            return socket_perror();
        break;
        case NET_ERR_INVALID_ARG: {
            const char *badarg = "Invalid argument!";
            LOGE(_err, "%s\n", badarg);
            return badarg;
        }
        break;
        case NET_ERR_NOT_PERMITTED: {
            const char *denied = "Operation is not permitted!";
            LOGE(_err, "%s\n", denied);
            return denied;
        }
        break;
        case NET_SUCCEEDED:
        default: {
            const char *success = "Operation succeeded!";
            LOGD(_err, "%s\n", success);
            return success;
        }
        break;
    }
}
