#ifndef __NETWORK_H__
#define __NETWORK_H__

#include <stdlib.h>
#include <stdint.h>
#include "log.h"
#include "socket.h"

#define NET_BUFFER_SIZE             (1500)              /* Network receive buffer */
#define NET_ADDRLEN                 (INET_ADDRSTRLEN)
#define NET_SUCCEEDED               (0x00000000)
#define NET_ERR_SOCKET_FAIL         (0xE0000000)
#define NET_ERR_INVALID_ARG         (0xE0000001)
#define NET_ERR_NOT_PERMITTED       (0xE0000002)
#define NET_ERR_CONN_CLOSED         (0xE0000004)

typedef void(*NetworkCallback)(void*);

typedef enum { 
    NetworkCient,
    NetworkServer,
    NetworkNull
} NetworkMode;

typedef struct {
    uint8_t *data;
    uint16_t len;
} NetworkBuffer;

typedef struct {
    /**
     *  When connection established, the network module will call on_connect()
    */
    NetworkCallback on_connect;

    /**
     *  When network is ready to send data, it will call on_writable()
     *  Since the data is not copied when sending, it may be lost if you make
     *  a next send before on_writable() called
    */
    NetworkCallback on_writable;

    /**
     *  When data is received, will call on_receive()
     *  The received data will be passed as an argument
    */
    NetworkCallback on_receive;

    /**
     *  When client or server disconnects, the network module will call on_disconnect()
    */
    NetworkCallback on_disconnect;

    /**
     *  When an error occurred in the network module, on_error() will be called
    */
    NetworkCallback on_error;
} NetworkCallbacks;

typedef struct {
    uint16_t port;
    char host[NET_ADDRLEN];
    NetworkMode mode;
    NetworkCallbacks cb;
} NetworkParam;

/**
 *  @brief Initializes socket subsystem and network module
 *
 *  @param param: network parameters - mode, host, port, callbacks
 * 
 *  @return If succeeded 1 will be returned, otherwise 0
 *
 *  @note To get error use network_gerror() or network_perror()
*/
int network_init(NetworkParam *param);

/**
 *  @brief Performs work on connect, disconnect, receive and send data
 *         network_poll() should be constantly called in your main loop
 * 
 *  @return If succeeded 1 will be returned, otherwise 0
 *
 *  @note To get error use network_gerror() or network_perror()
*/
int network_poll(void);

/**
 *  @brief Initializes socket subsystem and network module
 *
 *  @param data: pointer to the data to be sent
 *  @param len: size of data to be sent
 * 
 *  @return If succeeded 1 will be returned, otherwise 0
 *
 *  @note To get error use network_gerror() or network_perror()
 *        The data will not be copied, so you do not have to send 
 *        new data until the on_writable() called
*/
int network_send(uint8_t *data, uint16_t len);

/**
 *  @brief Disconnects from server or client
*/
int network_disconnect(void);

/**
 *  @brief Deinitializes socket subsystem and network module
*/
void network_deinit(void);

/**
 *  @brief Getting error code from network module
 *
 *  @return Error code
*/
const int network_gerror(void);

/**
 *  @brief Getting error string from network module
 * 
 *  @return Error string
*/
const char *network_perror(void);

#endif /* __NETWORK_H__ */
