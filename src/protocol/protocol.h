#ifndef __PROTOCOL_H__
#define __PROTOCOL_H__

#include <stdint.h>
#include <stdlib.h>
#include "log.h"
#include "protocol_util.h"

#define PACKET_HEAD                 MKTAG('p','t','a','g')
#define PACKET_HANDSHAKE            MKTAG('b','n','j','r')
#define PACKET_MINLEN               (8)
#define PACKET_MAXLEN               (1024)
#define PACKET_MAXDATALEN           (PACKET_MAXLEN - PACKET_MINLEN - 2)

#define PROTOCOL_SUCCEEDED          (0x00000000)
#define PROTOCOL_ERR_NULL_PTR       (0xE0000000)
#define PROTOCOL_ERR_MEM_ALLOC      (0xE0000001)
#define PROTOCOL_ERR_BAD_PACKET     (0xE0000002)

enum PacketType {
    /**
     *  Response to PKillPacket command
    */
    ResponsePacket,

    /**
     *  To a establish a connection the server and client
     *  must send handshakes to each other
    */
    ServerHSPacket, ClientHSPacket,

    /**
     *  Request to kill a process
    */
    PKillPacket,

    /**
     *  Request to get a list of processes
    */
    GetPLPacket,

    /**
     *  The number of processes in the system can be very large,
     *  we will split their sending into several parts,
     *  so we need additional packet types to identify the sending state
    */
    PLBeginPacket, PLChunkPacket, PLEndPacket,

    /**
     *  Dummy
    */
    NullPacket
};

typedef struct {
    uint32_t head;
    uint16_t type;
    uint16_t len;
    union {
        struct { char *data; } response;
        struct { uint32_t id; } handshake;
        struct { uint32_t pid; } kill_process;
        struct { char *data; } process_list;
    };
} Packet;

/**
 *  @brief Creates an empty Packet
 *
 *  @return If succeeded an instance of Packet will be returned, otherwise NULL
 *
 *  @note To get error use protocol_gerror() or protocol_perror()
*/
Packet *packet_create(void);

/**
 *  @brief Frees Packet instance
 *  
 *  @param packet: pointer to Packet instance
 * 
 *  @note Must be called for Packets created by packet_create()
*/
void packet_free(Packet *packet);

/**
 *  @brief Frees Packet instance with internal data
 *  
 *  @param packet: pointer to Packet instance
 *
 *  @note Must be called for Packets created by packet_unpack()
*/
void packet_free_all(Packet *packet);

/**
 *  @brief Сreates a sequential set of data to send over the network
 *  
 *  @param packet: pointer to Packet instance
 *
 *  @return If succeeded a serialized data will be returned, otherwise NULL
 *
 *  @note To get error use protocol_gerror() or protocol_perror()
*/
uint8_t *packet_pack(Packet *packet);

/**
 *  @brief Unpack a sequential set of data created with packet_pack()
 *  
 *  @param data: pointer to sequential set of data
 *  @param len: sizeof sequential set of data
 *
 *  @return If succeeded an unpacked Packet instance will be returned, otherwise NULL
 *
 *  @note To get error use protocol_gerror() or protocol_perror()
*/
Packet *packet_unpack(uint8_t *data, uint32_t len);

/**
 *  @brief Getting error code from protocol module
 *
 *  @return Error code
*/
const int protocol_gerror(void);

/**
 *  @brief Getting error string from protocol module
 * 
 *  @return Error string
*/
const char *protocol_perror(void);

#endif /* __PROTOCOL_H__ */
