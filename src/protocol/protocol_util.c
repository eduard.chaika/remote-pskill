#include "protocol_util.h"

void pack_u16(uint8_t **buf, uint16_t u) {
    *(*buf + 0) = u >> 8;
    *(*buf + 1) = (uint8_t)u;
    *buf += sizeof(uint16_t);
}

void pack_u32(uint8_t **buf, uint32_t u) {
    *(*buf + 0) = u >> 24;
    *(*buf + 1) = u >> 16;
    *(*buf + 2) = u >> 8;
    *(*buf + 3) = (uint8_t)u;
    *buf += sizeof(uint32_t);
}

void pack_string(uint8_t **buf, char *s) {
    uint16_t len = (uint16_t)strlen(s);
    pack_u16(buf, len);
    memcpy(*buf, s, len);
    *buf += len;
}

uint16_t unpack_u16(uint8_t **buf) {
    uint16_t result = ((uint16_t)(*buf)[0] << 8) | (*buf)[1];
    *buf += sizeof(uint16_t);
    return result;
}

uint32_t unpack_u32(uint8_t **buf) {
    uint32_t result = ((uint32_t)(*buf)[0] << 24) | ((uint32_t)(*buf)[1] << 16) | ((uint32_t)(*buf)[2] << 8) | (*buf)[3];
    *buf += sizeof(uint32_t);
    return result;
}

char *unpack_string(uint8_t **buf) {
    uint16_t len = unpack_u16(buf);
    char *result = (char*)malloc(len + 1);
    if (!result) return NULL;
    result[len] = '\0';
    memcpy(result, *buf, len);
    *buf += len;
    return result;
}
