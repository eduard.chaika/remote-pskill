#include "protocol.h"

#define set_error(code)     SET_ERROR(_err, code)
#define reset_error()       SET_ERROR(_err, PROTOCOL_SUCCEEDED)

static Error _err = { ._module = "protocol" };

static inline void set_packet_len(Packet *packet) {
    packet->len = sizeof(packet->head) + sizeof(packet->type) + sizeof(packet->len);

    switch (packet->type) {
        case ResponsePacket:
            if (packet->response.data) {
                /* Add 2 bytes from pack_string() */
                packet->len += (uint16_t)strlen(packet->response.data) + 2;
            }
        break;
        case ServerHSPacket:
        case ClientHSPacket:
            packet->len += sizeof(packet->handshake.id);
        break;
        case PKillPacket:
            packet->len += sizeof(packet->kill_process.pid);
        break;
        case GetPLPacket: break;
        case PLBeginPacket:
        case PLChunkPacket:
        case PLEndPacket:
            if (packet->process_list.data) {
                /* Add 2 bytes from pack_string() */
                packet->len += (uint16_t)strlen(packet->process_list.data) + 2;
            }
        break;
    }
}

Packet *packet_create(void) {
    reset_error();

    Packet *packet = (Packet*)calloc(1, sizeof(Packet));
    if (!packet) {
        set_error(PROTOCOL_ERR_MEM_ALLOC);
        return NULL;
    }

    packet->head = PACKET_HEAD;
    return packet;
}

void packet_free(Packet *packet) {
    if (packet) free(packet);
}

void packet_free_all(Packet *packet) {
    if (packet) {
        if (packet->type == ResponsePacket) {
            if (packet->response.data) free(packet->response.data);
        }
        else if (packet->type >= PLBeginPacket) {
            if (packet->process_list.data) free(packet->process_list.data);
        }
        free(packet);
    }
}

uint8_t *packet_pack(Packet *packet) {
    reset_error();

    uint8_t *_packet, *_ppacket;

    if (!packet) {
        set_error(PROTOCOL_ERR_NULL_PTR);
        return NULL;
    }

    if (packet->type >= NullPacket) {
        set_error(PROTOCOL_ERR_BAD_PACKET);
        return NULL;
    }

    set_packet_len(packet);
    if (packet->len > PACKET_MAXLEN) {
        set_error(PROTOCOL_ERR_BAD_PACKET);
        return NULL;
    }

    _packet = _ppacket = (uint8_t*)malloc(packet->len);
    if (!_packet) {
        set_error(PROTOCOL_ERR_MEM_ALLOC);
        return NULL;
    }

    pack_u32(&_ppacket, packet->head);
    pack_u16(&_ppacket, packet->type);
    pack_u16(&_ppacket, packet->len);

    switch (packet->type) {
        case ResponsePacket:
            if (packet->response.data) {
                pack_string(&_ppacket, packet->response.data);
            }          
        break;
        case ServerHSPacket:
        case ClientHSPacket:
            packet->handshake.id = PACKET_HANDSHAKE;
            pack_u32(&_ppacket, packet->handshake.id);
        break;
        case PKillPacket:
            pack_u32(&_ppacket, packet->kill_process.pid);
        break;
        case GetPLPacket: break;
        case PLBeginPacket:
        case PLChunkPacket:
        case PLEndPacket:
            if (packet->process_list.data) {
                pack_string(&_ppacket, packet->process_list.data);
            }
        break;
    }

    return _packet;
}

Packet *packet_unpack(uint8_t *data, uint32_t len) {
    reset_error();

    Packet *packet = NULL;
    uint8_t *_data = data;

    if (!data) {
        set_error(PROTOCOL_ERR_NULL_PTR);
        return NULL;
    }

    if (len < PACKET_MINLEN) {
        set_error(PROTOCOL_ERR_BAD_PACKET);
        return NULL;
    }

    packet = (Packet*)malloc(sizeof(Packet));
    if (!packet) {
        set_error(PROTOCOL_ERR_MEM_ALLOC);
        return NULL;
    }

    packet->head = unpack_u32(&_data);
    if (packet->head != PACKET_HEAD) {
        set_error(PROTOCOL_ERR_BAD_PACKET);
        goto exit_error;
    }

    packet->type = unpack_u16(&_data);
    if (packet->type >= NullPacket) {
        set_error(PROTOCOL_ERR_BAD_PACKET);
        goto exit_error;
    }

    packet->len = unpack_u16(&_data);
    if (packet->len > len) {
        set_error(PROTOCOL_ERR_BAD_PACKET);
        goto exit_error;
    }

    switch(packet->type) {
        case ResponsePacket:
            packet->response.data = unpack_string(&_data);
            if (!packet->process_list.data) {
                set_error(PROTOCOL_ERR_MEM_ALLOC);
                goto exit_error;
            }
        break;
        case ServerHSPacket:
        case ClientHSPacket:
            packet->handshake.id = unpack_u32(&_data);
        break;
        case PKillPacket:
            packet->kill_process.pid = unpack_u32(&_data);
        break;
        case GetPLPacket: break;
        case PLBeginPacket:
        case PLChunkPacket:
        case PLEndPacket:
            packet->process_list.data = unpack_string(&_data);
            if (!packet->process_list.data) {
                set_error(PROTOCOL_ERR_MEM_ALLOC);
                goto exit_error;
            }
        break;
    }

    return packet;

exit_error:
    free(packet);
    return NULL;
}

const int protocol_gerror(void) {
    return _err._code;
}

const char *protocol_perror(void) {
    switch(_err._code) {
        case PROTOCOL_ERR_NULL_PTR: {
            const char *nullptr = "Null pointer!";
            LOGE(_err, "%s\n", nullptr);
            return nullptr;
        }
        break;
        case PROTOCOL_ERR_BAD_PACKET: {
            const char *badpack = "Invalid packet!";
            LOGD(_err, "%s\n", badpack);
            return badpack;
        }
        break;
        case PROTOCOL_ERR_MEM_ALLOC: {
            const char *badalloc = "Memory allocation error!";
            LOGE(_err, "%s\n", badalloc);
            return badalloc;
        }
        break;
        case PROTOCOL_SUCCEEDED:
        default: {
            const char *success = "Operation succeeded!";
            LOGE(_err, "%s\n", success);
            return success;
        }
        break;
    }
}
