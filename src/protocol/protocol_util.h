#ifndef __PROTOCOL_UTIL_H__
#define __PROTOCOL_UTIL_H__

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define MKTAG(a, b, c, d)   (a | (b << 8) | (c << 16) | (d << 24))  /* Hello ffmpeg */

void pack_u16(uint8_t **buf, uint16_t u);

void pack_u32(uint8_t **buf, uint32_t u);

void pack_string(uint8_t **buf, char *s);

uint16_t unpack_u16(uint8_t **buf);

uint32_t unpack_u32(uint8_t **buf);

char *unpack_string(uint8_t **buf);

#endif /* __PROTOCOL_UTIL_H__ */
