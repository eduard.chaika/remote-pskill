cmake_minimum_required(VERSION 3.0.0)

set(NAME remote-pskill)
set(CLI_NAME "${NAME}-cli")
set(SRV_NAME "${NAME}-srv")

project(${NAME} VERSION 0.1.0)

set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -DDEBUG_LOG")
add_definitions(-DAPP_NAME="${NAME}")

set(CLI_SRC src/app/client src/app/generic)
set(SRV_SRC src/app/server src/app/generic)
set(MOD_LIST    src/platform/socket
                src/platform/system
                src/protocol
                src/network
)
set(OBJ_LIST    $<TARGET_OBJECTS:socket>
                $<TARGET_OBJECTS:system>
                $<TARGET_OBJECTS:protocol>
                $<TARGET_OBJECTS:network>
)

foreach(MOD_ITEM ${MOD_LIST})
    add_subdirectory(${MOD_ITEM})
endforeach()

add_executable(${CLI_NAME} ${OBJ_LIST} ${CLI_SRC})
set_target_properties(${CLI_NAME} PROPERTIES C_STANDARD 11 C_STANDARD_REQUIRED YES)
target_include_directories(${CLI_NAME} PUBLIC src/common ${MOD_LIST})

add_executable(${SRV_NAME} ${OBJ_LIST} ${SRV_SRC})
set_target_properties(${SRV_NAME} PROPERTIES C_STANDARD 11 C_STANDARD_REQUIRED YES)
target_include_directories(${SRV_NAME} PUBLIC src/common ${MOD_LIST})
