# Remote pskill

Client/Server application to close processes on remote Host
- Client gets processes list and sends them by server request.
- Server will request to close one of the processes on client's host.
- Client has to reply with success or failure.

# Building

_CMake_ 3.0.0 or _higher_ is _required_.

1. Clone repository to your local machine:

        git clone https://gitlab.com/eduard.chaika/remote-pskill.git
2. Configure application:
        
        cd remote-pskill
        mkdir build
        cd build
        cmake .. -DCMAKE_BUILD_TYPE=Release

    > Note: You can build Debug or Release versions, just specify -DCMAKE_BUILD_TYPE

    > Note: In the Debug version you will see additional log messages
3. Build application:

        make
4. Remote-pskill application binary file now in *remote-pskill/build* folder.

# Usage

## Client
    
### Linux
In bash terminal type:

    admin@desktop:~$ <path-to-file>\remote-pskill-cli <host> <port> <log>

### Windows
Open your favorite command-line tool and type:

    C:\Users\Admin> <path-to-file>\remote-pskill-cli.exe <host> <port> <log>

_host_ - server host address

_port_ - server port in range from 1024 to 65535

_log_ - set '-q' for silent mode

## Server

### Linux
In bash terminal type:

    admin@desktop:~$ <path-to-file>\remote-pskill-srv <port>

### Windows
Open your favorite command-line tool and type:

    C:\Users\Admin> <path-to-file>\remote-pskill-srv.exe <port>

_port_ - server port in range from 1024 to 65535

### Commands

First of all, make sure the client is connected, you should see a message:

    Client connected!

If client is connected you can enter commands:

1. To get a list of processes on the remote host, type:

        ps

2. To kill process on the remote host, type:

        kill <pid>

_pid_ - process ID resulting from the command _ps_
